from flask import Flask, jsonify, request


app = Flask(__name__)


@app.route('/'):
def home():
    return jsonify(data='Welcome to flasky, my first Flask app on a publically accessible repo!')

if __name__ == '__main__':
    app.run()